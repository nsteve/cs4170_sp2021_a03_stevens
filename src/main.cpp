#include <iostream>
#include <iomanip>
#include <math.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>

#include <random>
#include "CStopWatch.h"
#include <omp.h>
#include <boost/mpi.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

#define PI_F 3.141592654f

namespace mpi = boost::mpi;
namespace mt = mpi::threading;

typedef std::vector<int>       iArray1D;
typedef std::vector<double>    ldArray1D;
typedef std::vector<ldArray1D> ldArray2D;
typedef std::vector<ldArray2D> ldArray3D;


std::random_device rd;                          // only used once to initialise (seed) engine
double randDbl(const double& min, const double& max) {
    static thread_local std::mt19937* generator = nullptr;
    if (!generator) {
        generator = new std::mt19937(clock());
    }
    std::uniform_real_distribution<double> distribution(min, max);
    return distribution(*generator);
}

double Rastrigin(std::vector<double> &R, int Nd, int p){ // Rastrigin
    double Z = 0, Xi;

    for (int i = 0; i < Nd; i++){
        Xi = R[i];
        Z += (pow(Xi, 2) - 10 * cos(2 * PI_F * Xi) + 10);
    }
    return -Z;
}

class Particle{
public:
    Particle() : Particle(0){}

    Particle(int Nd){
        R.resize(Nd, -INFINITY);
        V.resize(Nd, -INFINITY);
        pBestPos.resize(Nd, -INFINITY);

        M = -INFINITY;
        pBest = -INFINITY;
    }

    void print(){
        std::cout << "M: " << M << "\n";
        std::cout << "pBest: " << pBest << "\n";

        for (int i = 0; i < (int)R.size(); i++){
            std::cout << "R[" << i << "] = " << R[i] << "\n";
        }
        for (int i = 0; i < (int)V.size(); i++){
            std::cout << "V[" << i << "] = " << V[i] << "\n";
        }
    }

    template <typename Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar &R;
        ar &V;
        ar &M;
        ar &pBest;
        ar &pBestPos;
    }

    // private:
    friend class boost::serialization::access;

    std::vector<double> R;
    std::vector<double> V;
    double M;
    double pBest;
    std::vector<double> pBestPos;
};

class Swarm{

    public:
        Swarm() : Swarm(0, 0) {}
        Swarm(int Np, int Nd){
            particles.resize(Np, Particle(Nd));
        }

        void print(){
            for (int i = 0; i < (int)particles.size(); i++){
                particles[i].print();
            }
        }

        template <typename Archive>
        void serialize(Archive &ar, const unsigned int version){
            ar &particles;
        }

        friend class boost::serialization::access;
        std::vector<Particle> particles;
};

double f(double x)
{
    return x * x;
}

void magic(int Np, int Nd, int numThreads, int trial)
{
    mpi::communicator world;
    CStopWatch timer;

    int numProcs, myRank, myNp,  Nt, numEvals;
    double xMin, xMax, vMin, vMax;

    std::vector<Particle> myBestParticles;
    std::vector<Particle> myParticles;

    //Nd = 30;
    Nt = 6000;
    numEvals = 0;

    xMin = -5.12;   // Lower Bound
    xMax = 5.12;    // Upper Bound
    vMin = -1;      // Minimum Velocity
    vMax = 1;       // Maximum Velocity

    float C1 = 1.45, C2 = 1.45;
    float w, wMax = 0.9, wMin = 0.1;
    float R1, R2;

    //Np = 16;

    //Initializes particles
    Swarm S(Np, Nd);
    Particle myBestP(Nd);

    numProcs = world.size();
    myRank = world.rank();
    
    myNp = Np/numProcs;
    myParticles.resize(myNp);
    myBestParticles.resize(numProcs);

    mpi::scatter(world, S.particles, myParticles.data(), myNp, 0);

    //Global best values
    ldArray1D gBestPosition(Nd, -INFINITY);
    float gBestValue = -INFINITY;

    if(myRank == 0) {
        
        timer.startTimer();
    }

    omp_set_num_threads(numThreads);
    #pragma omp parallel
    {
        //Init Population
        #pragma omp for collapse(2)
        for (int p = 0; p < myNp; p++){
            for (int i = 0; i < Nd; i++){
                myParticles[p].R[i] = randDbl(xMin, xMax);
                myParticles[p].V[i] = randDbl(xMin, xMax);
            }
        }
    
        //Evaluate Fitness
        #pragma omp for reduction(+: numEvals)
        for (int p = 0; p < myNp; p++){
            myParticles[p].M = Rastrigin(myParticles[p].R, Nd, p);
            numEvals++;
        }
    }
    
    MPI_Allreduce(&numEvals, &numEvals, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    for (int j = 1; j < Nt; j++){
        
        omp_set_num_threads(numThreads);
        #pragma omp parallel
        {
            //omp_set_num_threads(numThreads);
            #pragma omp for collapse(2)
            for (int p = 0; p < myNp; p++){ // Particle
                for (int i = 0; i < Nd; i++){ // Dimension
                    myParticles[p].R[i] = myParticles[p].R[i] + myParticles[p].V[i]; // Update my position
    
                    // Corrects a particle if it's outside the bounds
                    if (myParticles[p].R[i] > xMax){ myParticles[p].R[i] = randDbl(xMin, xMax);}
                    if (myParticles[p].R[i] < xMin){ myParticles[p].R[i] = randDbl(xMin, xMax);}
                }
            }
            
            //omp_set_num_threads(numThreads);
            #pragma omp for reduction(+: numEvals)
            for (int p = 0; p < myNp; p++){
                myParticles[p].M = Rastrigin(myParticles[p].R, Nd, p);
                numEvals++;
            }
        
            #pragma omp single nowait
            {
                MPI_Allreduce(&numEvals, &numEvals, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
            }
            
    
            // Global & Personal Bests
            #pragma omp for
           for(int p = 0; p < myNp; p++)
           {
               if(myParticles[p].M > myBestP.M) {
                   myBestP.M = myParticles[p].M;
                   //std::cout << myBestP.M << std::endl;
                   for (int i = 0; i < Nd; i++){
                        myBestP.pBestPos[i] = myParticles[p].R[i];
                    }
               }
    
               //local
               if(myParticles[p].M > myParticles[p].pBest)
               {
                   myParticles[p].pBest = myParticles[p].M;
                    for (int i = 0; i < Nd; i++){
                        myParticles[p].pBestPos[i] = myParticles[p].R[i];
                    }
               }
    
           }
        }
           
        

                mpi::gather(world, myBestP, myBestParticles, 0);

    
            if(myRank == 0)
            {
                for(int i = 0; i < (int)myBestParticles.size(); i++) {
                    if(myBestParticles[i].M > gBestValue)
                    {
                        //std::cout << myBestParticles[i].M << std::endl;
                        gBestValue = myBestParticles[i].M;
                        for (int j = 0; j < Nd; j++){
                            gBestPosition[j] = myBestParticles[i].R[j];
                        }
                    }
                }
            }
        
        

        mpi::broadcast(world, gBestValue, 0);
        mpi::broadcast(world, gBestPosition, 0);

        if (gBestValue >= -0.0001){
            break;
        }

        
        // Update Velocities
        w = wMax - ((wMax - wMin) / Nt) * j;
        omp_set_num_threads(numThreads);
        #pragma omp parallel for private(R1, R2) collapse(2)
        for (int p = 0; p < myNp; p++){
            for (int i = 0; i < Nd; i++){
                R1 = randDbl(0, 1);
                R2 = randDbl(0, 1);

                // Original PSO
                myParticles[p].V[i] = w * myParticles[p].V[i] + C1 * R1 * (myParticles[p].pBestPos[i] - myParticles[p].R[i]) + C2 * R2 * (gBestPosition[i] - myParticles[p].R[i]);
                if (myParticles[p].V[i] > vMax){ myParticles[p].V[i] = randDbl(vMin, vMax);}
                if (myParticles[p].V[i] < vMin){ myParticles[p].V[i] = randDbl(vMin, vMax);}
            }
        }

    }

    world.barrier();
    if(myRank == 0) {
        timer.stopTimer();
        std::cout << "Trial: " << trial << " " 
        << gBestValue      << " "
        << Np              << " "
        << Nd              << " "
        << numEvals        << " "
        << numProcs        << " "
        << numThreads      << " "
        << timer.getElapsedTime()       << std::endl;
    }

}

int main(){
    
    int NpMin, NpMax, NpStep, Np, Nd, numTrials, numThreadsMax, numThreads, trial;
    NpMin = 100; NpMax = 1000; NpStep = 100;
    Nd = 30;
    numTrials = 5;
    numThreadsMax = 10;
    
    mpi::environment env(mt::funneled);

    if(env.thread_level() < mt::funneled){
        env.abort(-1);
    }

    for(trial = 0; trial < numTrials; trial++)
    {
        for(numThreads = 1; numThreads <= numThreadsMax; numThreads++)
        {
            for(Np = NpMin; Np <= NpMax; Np += NpStep)
            {
                magic(Np, Nd, numThreads, trial+1);
            }
        }
    }

    return 0;
}

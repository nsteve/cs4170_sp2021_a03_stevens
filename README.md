# Assignment \#03 #

Completed by Noah Stevens

## Summary/Algorithm ##
The code written in main.cpp is the parallelized version of the Particle Swarm Optimization (PSO) algorithm using MPI (Message Passing Interface) with OpenMP. The Particle swarm optimization algorithm is used to find the best solution to a problem by creating a population of "particles" that represent different solutions to that problem. PSO applies "velocity" to these particles based on information they collect (best global solution, best individual solution, etc.). They collect information by trying different solutions to the given equation. This information is gathered to create better educated guesses that particles use to get closer to solving a  problem. Initially the particles are made to move around a lot to "search out" the best solution. As time continues, the particles move more slowly and have less randomness, closing in on an answer. In other words, the particles in the swarm are being optimized.  

The funtion that the PSO algorithm was trying to find the minimum of (smallest number) Rastrigin function. This function is "used as a performance test problem for optimization algorithms" (Wiki), and its minimum is zero. The particles in the PSO algorithm were attempting to find zero, f(x) = 0. Here is the equation:

![Rastrigin Function](./Results/rastrigin.png "Rastrigin Function")

As I've stated above, the code was parallelized using MPI and OpenMP. We are going to call the combination of both parallelization methods a "hybrid" program. 

MPI is simply an interface designed so that a single program can communicate across multiple "nodes". Nodes may be different computers, processors, or cores depending on where the program is run and how it is executed. Usually, the program is run by these nodes at the same, but by using MPI, one can programmatically divide work between nodes. MPI allows these nodes to communicate with one another, enabling more complex programs and cooperation between nodes. When tasks are split up between nodes, tasks can be completed concurrently, which improves speed and sometimes performance of a given program.

OpenMP uses a team threads to divide up tasks. A team of threads (individual processes) was used to update particles at the "same time" as opposed to updating one particle at a time using serial code. This code used OpenMP to initialize position and fitness, update positions, evaluate fitness, and update velocities of multiple particles concurrently. These operations are mostly completed by for-loops which makes implementing OpenMP quite easy. OpenMP will take each iteration of a for-loop and spread out its operations between the threads it is given. For example, if a for loop has 4 iterations and there are 2 threads, then OpenMP will give 2 iterations to each thread. Luckily, there were very few loop dependencies to worry about in this code. Loop dependencies make parallelization with OpenMP more difficult, because one iteration of a loop will depend of the previous one. 

Because this code is hybridized (using both MPI and OpenMP), parallelization can be maximized. MPI allows for individual particles to be spread between nodes and OpenMP allows particles in each node to be "processed" concurrently. Hybridization allows do a massive number of computations at the same time, which can dramatically increase the speed of complex programs. 

This code could potentially be more optimized. There is some extra memory being used because every node is creating a whole swarm, which isn't needed. There are also some MPI calls that I used that may not be as efficient as alternatives. For example, I broadcast both gBestVaue and gBestPosition, when I could have just broadcasted the one node or array that I had created and finished the computations on each Node. This would save communication time. Using OpenMP, one could potentially implement nested parallelism to increase optimization. I decided not to implement nested parallelization, as it would have required the use of many more cores and did little in achieving my main goal of parallelizing the particle swarm optimization algorithm. 

## Method ##
To complete this assignment, I took the code PSO code that I parallelized using MPI and implemented OpenMP the same way I had in our first assignment. Because MPI sends the same serial code to each node, I knew I could implement OpenMP as if I was looking at serial code. The only places where I couldn't use this method was when there were MPI calls. When there were MPI calls I used "#pragma omp single nowait" so only thread thread would execute this code while other threads continued to do work.  When completing this assignment, I also found that my code ran more slowly when I attempted to parallelize the computation of global best variables. The calculation of the global best value and position is the least efficient part of my code. 

**Method for MPI**

Before doing any programming for this assignment, I did a lot of thinking about how I wanted this PSO algorithm to function across multiple nodes. I thought a lot about what it meant when all the nodes were going to execute the same program and how they would communicate with one another (minimally). I knew that I wanted to create one large swarm and then divide the particles into each node using MPI_Scatter. Once those nodes were divided up, then almost the entire program could continue running normally without much modification. This solution had the benefit of being simple and effective. Hindsight being 20/20, I now realize that I had initiated a swarm in each process, but this could easily be fixed by having the master node create the swarm and then scatter the particles before it started. 

The last important piece of the code that I had to figure out was how I was going to update the global variables. The solution I used was more complicated than it needed to be, but it was simple, and it will be easy to improve. The idea I had was to use MPI_Broadcast to update the gBestPosition and gBestValue in all nodes. To find the gBestPosition and gBestValue, I had each node send the master node its best node. Once the master had received all the nodes it compared them, updated the variables, and sent them via broadcast. Next time I did this assignment I would probably use MPI_Allreduce to and the MPI_MAX operation to simply have MPI find and replace the best global values. 

**Method for OpenMP**

The main method that was used to complete this assignment was to search out areas in the PSO function, found within main.cpp, that were repeated many times, loops. Once loops were found, they needed to be checked for loop dependencies. If many loop dependencies were found, then that loop may not be good for parallelization. If there were no loop dependencies, then I attempted to parallelize the loop while being careful to make sure that variables were set to private when they were required to be (variables are usually set to private when multiple threads assign values to that variable at the same time). Other modifiers were added to the #pragma omp parallel statement as needed (reduction for adding iterations into one variable, collapse for nested for loops).

When completing this assignment, I began by parallelizing the piece of the program that took the longest: updating velocity. I used a simple #pragma omp parallel statement and made sure to privatize R1 and R2, as these variables were being assigned a new random number each iteration. If these variables were not private, then the program may not complete because different threads may attempt to assign values to this variable at the same time. Since this code was made of nested for loops, I also used a collapse statement to improve parallelization. 

I continued parallelizing the code by looking to the loops above the code that updated velocity. I applied similar strategies as the one described above, but I decided to create the largest #pragma omp parallel {} statement I could, so teams of threads would not be generated and then removed more times than necessary. Inside this parallel section I used #pragma omp for statements to parallelize other loops. Sections of code that needed to be executed by only one thread (serial code) I enclosed with a #pragma omp single. Some pieces of code could not be included in the parallel section, the break; statement, so the whole thing could not be paralyzed without more significant modification. 

I used default scheduling for the loops. When tested in previous assignments, utilizing different loop scheduling did not make a large difference in performance, but that result is likely due to the size and complexity of our programs. Testing different kinds of loop scheduling types would have increased the time of testing by many hours. I thought my time would be better suited making sure that the code was parallelized correctly. 

## Instructions ##

This program requires gcc, MPI, OpenMp, and boost to be installed on the users system. Because I had some trouble installing some of these features, I used a docker container that had them installed to run the program on my local computer. 
Run these commands to use the docker container where X is the number of nodes you want to use.

```
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all  
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp mpiexec --allow-run-as-root -n X MPI
```

One can run this program by navigating to the ./Default directory and executing the make file and then the resulting MPI file using these commands where X is the number of nodes you want to use. 

```
make all 
mpiexec -n X MPI
```

One may also use g++ to compile main.cpp in the ./src directory, but flags for MPI, OpenMP, and boost will need to be used (see makefile).

To run on OSC (preferred method) one must submit a job script using a slurm file on the OSC. One can see the job script [here](./jobScript.slurm).

## Algorithm ##

**Stated from above:**
The Particle swarm optimization algorithm is used to find the best solution to a problem by creating a population of "particles" that represent different solutions to that problem. PSO applies "velocity" to these particles based on information they collect (best global solution, best individual solution, etc.). They collect information by trying different solutions to the given equation. This information is gathered to create better educated guesses that particles use to get closer to solving a  problem. Initially the particles are made to move around a lot to "search out" the best solution. As time continues, the particles move more slowly and have less randomness, closing in on an answer. In other words, the particles in the swarm are being optimized.

The equation that the PSO algorithm was using was the Rastrigin function. This function is "used as a performance test problem for optimization algorithms" (Wiki), and its minimum is zero. The particles in the PSO algorithm were attempting to find zero, f(x) = 0. (See image of equation above).

**Pseudocode for the PSO algorithm:**
![Particle Swarm Pseudocode](./Results/psuedoPSO.png "Particle Swarm Pseudocode")

**Additional Description**
The PSO algorithm is said to behave like a flock of birds looking for food and is representative of a "simplified social system". The particles communicate with one another via data that they share and continue to update. Particles tend to follow other particles with the best answer, while also being influenced by other factors. 

This implementation of the algorithm retires particles if they reach the correct answer, and it also places out of bounds particles in a random position within the defined area. 

## Results - Discussion and Analysis ##
All results are available in [CSV](./Results/results.csv) and [Excel](./Results/Results_Hybrid.xlsx) format. Performance was measured using the average of 5 trials of the Particle Swarm Optimization Algorithm. Each trial was divided to test 1-10 nodes and 1-10 threads with a data size 100 to 1000 particles with each step being 100 particles. Average values are found in the excel document. Raw data can be found in the .csv file. Tables of speedup, Efficiency, and the Karp-Flatt metric are available below.  

Speedup was linear for most data sizes, but it was sublinear for the smallest two data sizes. Sublinear speedup for these small data sizes was expected because particles were divided up so much that each thread only held one particle. This level of parallelization causes low speedup because the work being done by each thread is so small. At this point, increasing a thread's workload does not slow down the program. These results indicate that the program was probably parallelized correctly, and that more data typically leads to higher amounts of speedup (see the lines with higher Np - Np is denoted by the different colored lines - are more linear than ones with low Np).

Speedup using hybrid parallelization is similar to the speedup gained using just MPI and OpenMP using this method of calculating speedup. To determine the number of processes running, I multiplied the number of nodes by the number of threads. By calculating the number of processes this way, one should expect all methods to be similar. If one was to compare each method (hybrid, only OpenMP, and only MPI) by only using the number of nodes used in MPI or only the number of threads used in OpenMP, while disregarding the other method when calculating number of processes, then the speedup of the hybrid method would be up to 10 times the speedup of the other two methods. Graphs and results of each method can be found in the Results folder. 

Average efficiency started at 1.0 and declined linearly for every Np tested. The negative slope of this decline increased (closer to 0) when more Np was used. This result indicates that this program is most efficient when it is processing very large amounts of data, which is not surprising. Efficiency would likely be higher if more data was used, but the negative slope may indicate a lack of scalability. Efficiency could be closer to 1.0 if optimizations in the code were made that were described earlier in the reflection. The results indicate that large data sets can utilize many cores the most efficiently, while small data sets are most efficient using fewer cores. 

Efficiency was worse using hybridization than the other two methods. When combining both MPI and OpenMP there are more places where inefficiencies can occur. One may be able to increase inefficiency caused by code by altering the code to decrease the number of wasted threads and extra MPI calls. Another reason why efficiency is worse when using this method because work is more split-up than it is using the other two methods, which causes inefficiency. One could add more particles to increase efficiency. 

The Karp-Flatt Metric was low for almost all data sizes and number of processes except for Np of 100. This Np was not very parallelized because it was putting only 1 or 2 particles on each node and/or thread, which is not effective parallelization. In some cases, it may take longer than the serial code. This result indicates that the amount of parallelized code in this program was high and it was parallelized well. Improvements mentioned earlier in this reflection could get this metric even lower. One can see that the most parallelization happened when the nodes and Np was maxed out, which is expected. 

The Karp-Flatt metric was its lowest when only OpenMP was used. This result is not very surprising because OpenMP can parallelize most of the PSO code very efficiently. MPI splits the particles between nodes and requires communication between the nodes, which can slow down the program. Combining the two creates a better result than using MPI on it's own, and improvements in efficiency could result in an even lower karp-flatt metric. A low karp-flatt metric (close to zero) indicates highly parallelized code. 


<div style="align:center;">

![2D Speedup](./Results/speedup01_hybrid.png "2D Speedup")
![3D Speedup](./Results/speedup02_hybrid.png "3D Speedup")

![2D Efficiency](./Results/efficiency01_hybrid.png "2D Efficiency")
![3D Efficiency](./Results/efficiency02_hybrid.png "3D Efficiency")

![2D Karp-Flatt](./Results/karpflatt01_hybrid.png "2D Karp-Flatt")
![3D Karp-Flatt](./Results/karpflatt02_hybrid.png "3D Karp-Flatt")
</div>


